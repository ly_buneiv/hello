<?php

namespace Lybuneiv\Hello;

use Illuminate\Support\ServiceProvider;

class HelloServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadRoutesFrom(__DIR__.'/routes.php');
		$this->loadViewsFrom(__DIR__.'/views', 'Hello');
		$this->publishes([
			__DIR__.'/routes.php',
			__DIR__.'/views', 'Hello'

		]);

	}
	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->make('Lybuneiv\Hello\Controllers\HelloController');
	}
}