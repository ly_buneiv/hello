<?php

namespace Lybuneiv\Hello\Controllers;

use App\Http\Controllers\Controller;

class HelloController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\View\Factory | \Illuminate\View\View
	 */
	public function index()
	{
		return view('Hello::index');
	}

}